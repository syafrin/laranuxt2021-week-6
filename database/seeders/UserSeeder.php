<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'password' => bcrypt('12345678'),
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'employee_id' => 1
        ]);
    }
}
