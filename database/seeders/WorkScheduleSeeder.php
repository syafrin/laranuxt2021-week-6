<?php

namespace Database\Seeders;

use App\Models\WorkSchedule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class WorkScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkSchedule::create([
            'checkin_time' => '08:00:00',
            'break_start_time' => '12:00:00',
            'break_end_time' => '13:45:00',
            'checkout_time' => '17:00:00',
            'name'=> 'senin'
        ]);
    }
}
