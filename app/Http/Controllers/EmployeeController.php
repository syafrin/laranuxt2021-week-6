<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp = Employee::all();
        $count = Employee::count();
        return response()->json([
            'data' => $emp,
            'count' => $count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        if(!$user->hasPermissionTo('user.create')){
            abort(403);
        }
        if (empty($request->name)) {
            return response()->json([
                'success' => false,
                'message' => "Nama wajib diisi"
            ]);
        }
        if (empty($request->date_of_birth)) {
            return response()->json([
                'success' => false,
                'message' => "Tanggal lahir wajib diisi"
            ]);
        }
        if (empty($request->job_title)) {
            return response()->json([
                'success' => false,
                'message' => "Jabatan wajib diisi"
            ]);
        }
        if (strlen($request->name) < 4) {
            return response()->json([
                'success' => false,
                'message' => "Nama minimal 4 huruf"
            ]);
        }

        $emp = new Employee;
        $emp->name = $request->name;
        $emp->date_of_birth = $request->date_of_birth;
        $emp->job_title = $request->job_title;
        $emp->unit_id = 1;
        $emp->profiles = [];
        $emp->save();
        return response()->json([
            'success' => true,
            'data' => $emp
        ]);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //$emp = Employee::find($id);
        return response()->json([
            'status' => true,
            'data' => $employee
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->name = $request->name;
        $employee->date_of_birth = $request->date_of_birth;
        $employee->job_title = $request->job_title;
        $employee->save();
        return response()->json([
            'success' => true,
            'data' => $employee
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        return $employee->delete();
        return response()->json([
            'success' => true
            ]);
    }
}
